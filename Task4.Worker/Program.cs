﻿using System;
using Task4.Worker.Services;

namespace Task3.Worker
{
	class Program
	{
		static void Main(string[] args)
		{
			MessageService s = new MessageService();
			s.Run();

			Console.ReadLine();
			s.Dispose();
		}
	}
}
