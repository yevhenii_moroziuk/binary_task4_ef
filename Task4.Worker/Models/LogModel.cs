﻿using System;

namespace Task4.Worker.Models
{
	class LogModel
	{
		public DateTime WrittenData { get; set; }
		public string Message { get; set; }
	}
}
