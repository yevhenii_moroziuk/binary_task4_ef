﻿using System;

namespace Task4.Client.Models
{
	public class Task
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime Created_At { get; set; }
		public DateTime Finished_At { get; set; }
		public TaskState State { get; set; }
		public int? Project_Id { get; set; }
		public int? Performer_Id { get; set; }

		public override string ToString()
		{
			return $"Id: {Id}, Name: {Name}, Description: {Description}, Created_At: {Created_At.ToShortDateString()}," +
				   $"Finished_At: {Finished_At}, State: {State}," +
				   $" Project_Id: {Project_Id}, Performer_Id: {Performer_Id}\n";
		}
	}
}
