﻿namespace Task4.Client.Models
{
	class Task7
	{
		public Project Project { get; set; }
		public Task TaskByDescription { get; set; }
		public Task TaskByLength { get; set; }
		public int CountOfUsers { get; set; }
	}
}
