﻿using System;

namespace Task4.Client.Models
{
	class LogModel
	{
		public DateTime WrittenData { get; set; }
		public string Message { get; set; }
	}
}
