﻿using System;

namespace Task4.Client.Models
{
	public class User
	{
		public int Id { get; set; }
		public string First_Name { get; set; }
		public string Last_Name { get; set; }
		public string Email { get; set; }
		public DateTime Birthday { get; set; }
		public DateTime Registered_At { get; set; }
		public int? Team_Id { get; set; }

		public override string ToString()
		{
			return $"Id {Id}, Name: {First_Name} {Last_Name}, Email: {Email}, Birthday: {Birthday.ToShortDateString()}," +
				   $"Registered_At: {Registered_At.ToShortDateString()}, Team_Id: {Team_Id}\n";
		}
	}
}
