﻿namespace Task4.Client.Models
{
	class Task6
	{
		public User User { get; set; }
		public Project LastProject { get; set; }
		public int TaskCount { get; set; }
		public int UnfinishedTaskCount { get; set; }
		public Task LongestTask { get; set; }
	}
}
