﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Task4.Client.Helpers;
using Task4.Client.Services;
using Task = System.Threading.Tasks.Task;

namespace Task4.Client
{
	class Program
	{
		static async Task Main(string[] args)
		{
			var services = new ServiceCollection();
			services.AddSingleton<IDataClient>(new DataClient());

			var serviceProvider = services.BuildServiceProvider();

			DataService service  = new DataService(serviceProvider.GetService<IDataClient>());

			ClientHub hub = new ClientHub();
			bool IsFinished = false;

			Helper.ShowGeneralInfo();
			await hub.WorkAsync();

			while (!IsFinished)
			{

				switch (Helper.InputInteger(1, 9))
				{
					case 1:
						await service.Task1Async(50);
						break;
					case 2:
						await service.Task2Async(30);
						break;
					case 3:
						await service.Task3Async(50);
						break;
					case 4:
						await service.Task4Async();
						break;
					case 5:
						await service.Task5Async();
						break;
					case 6:
						await service.Task6Async(50);
						break;
					case 7:
						await service.Task7Async(7);
						break;
					case 8:
						await service.Task8Async();
						break;
					case 9:
						IsFinished = true;
						break;
					default:
						await hub.StopAsync();
						break;
				}
			}
			Console.ReadKey();
		}
	}
}
