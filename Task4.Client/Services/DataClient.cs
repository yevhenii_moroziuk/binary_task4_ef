﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Task4.Client.Services
{
	public class DataClient:IDataClient
	{
		private HttpClient _client;
		private readonly string _base;

					

		public DataClient()
		{
			_client = new HttpClient();

			var builder = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.Build();

			_base = "https://localhost:44376/api/prtask/";//builder["BaseURl"];//
		}

		public async Task<T> GetAsync<T>(string specificAddres)
		{
			var ans = await _client.GetStringAsync(_base + specificAddres);
			return JsonConvert.DeserializeObject<T>(ans);
		}
	}
}
