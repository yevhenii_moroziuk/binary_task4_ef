﻿using System.Threading.Tasks;

namespace Task4.Client.Services
{
	public interface IDataClient
	{
		Task<T> GetAsync<T>(string specificAddres);
	}
}
