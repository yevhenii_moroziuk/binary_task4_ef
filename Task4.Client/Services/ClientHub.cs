﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading.Tasks;

namespace Task4.Client.Services
{
	public class ClientHub
	{
		private HubConnection _hubConnection;

		public ClientHub()
		{
			_hubConnection = new HubConnectionBuilder()
						.WithUrl("https://localhost:44376/hubmes")
						.Build();
		}			


		public async Task WorkAsync()
		{
			await _hubConnection.StartAsync();

			_hubConnection.On<string>("SendMessage", (message) => { Console.WriteLine(message); });
		}

		public async Task StopAsync()
		{
			await _hubConnection.StopAsync();
		}
	}
}
