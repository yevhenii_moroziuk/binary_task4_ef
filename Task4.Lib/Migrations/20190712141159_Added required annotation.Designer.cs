﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Task4.Lib.DataAccess;

namespace Task4.Lib.Migrations
{
    [DbContext(typeof(BinaryDataContext))]
    [Migration("20190712141159_Added required annotation")]
    partial class Addedrequiredannotation
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.2-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataAccess.Models.Project", b =>
                {
                    b.Property<int>("Id");

                    b.Property<int?>("Author_Id")
                        .HasColumnName("Author_Id");

                    b.Property<DateTime>("Created_At")
                        .HasColumnName("Created_At");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int?>("Team_Id")
                        .HasColumnName("Team_Id");

                    b.HasKey("Id");

                    b.HasIndex("Author_Id");

                    b.HasIndex("Team_Id");

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("DataAccess.Models.Task", b =>
                {
                    b.Property<int>("Id");

                    b.Property<DateTime>("Created_At")
                        .HasColumnName("Created_At");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<DateTime>("Finished_At")
                        .HasColumnName("Finished_At");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int?>("Performer_Id")
                        .HasColumnName("Performer_Id");

                    b.Property<int?>("Project_Id")
                        .HasColumnName("Project_Id");

                    b.Property<int>("State");

                    b.HasKey("Id");

                    b.HasIndex("Performer_Id");

                    b.HasIndex("Project_Id");

                    b.ToTable("Tasks");
                });

            modelBuilder.Entity("DataAccess.Models.Team", b =>
                {
                    b.Property<int>("Id");

                    b.Property<DateTime?>("Created_At")
                        .HasColumnName("Created_At");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("DataAccess.Models.User", b =>
                {
                    b.Property<int>("Id");

                    b.Property<DateTime>("Birthday");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("First_Name")
                        .IsRequired()
                        .HasColumnName("First_Name");

                    b.Property<string>("Last_Name")
                        .IsRequired()
                        .HasColumnName("Last_Name");

                    b.Property<DateTime>("Registered_At")
                        .HasColumnName("Registered_At");

                    b.Property<int?>("Team_Id")
                        .HasColumnName("Team_Id");

                    b.HasKey("Id");

                    b.HasIndex("Team_Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("DataAccess.Models.Project", b =>
                {
                    b.HasOne("DataAccess.Models.User", "Author")
                        .WithMany("Projects")
                        .HasForeignKey("Author_Id");

                    b.HasOne("DataAccess.Models.Team", "Team")
                        .WithMany("Projects")
                        .HasForeignKey("Team_Id");
                });

            modelBuilder.Entity("DataAccess.Models.Task", b =>
                {
                    b.HasOne("DataAccess.Models.User", "Performer")
                        .WithMany("Tasks")
                        .HasForeignKey("Performer_Id");

                    b.HasOne("DataAccess.Models.Project", "Project")
                        .WithMany("Tasks")
                        .HasForeignKey("Project_Id");
                });

            modelBuilder.Entity("DataAccess.Models.User", b =>
                {
                    b.HasOne("DataAccess.Models.Team", "Team")
                        .WithMany("Users")
                        .HasForeignKey("Team_Id");
                });
#pragma warning restore 612, 618
        }
    }
}
