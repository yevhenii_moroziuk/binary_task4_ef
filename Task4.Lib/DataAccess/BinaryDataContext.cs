﻿using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Task4.Lib.DataAccess
{
	public class BinaryDataContext:DbContext
	{
		public BinaryDataContext(DbContextOptions<BinaryDataContext> options)
			: base(options)
		{
		}

		public virtual DbSet<Project> Projects { get; set; }
		public virtual DbSet<Task> Tasks { get; set; }
		public virtual DbSet<Team> Teams { get; set; }
		public virtual DbSet<User> Users { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10062");

			modelBuilder.Entity<Project>(entity =>
			{
				entity.Property(e => e.Id).ValueGeneratedNever();

				entity.Property(e => e.Author_Id).HasColumnName("Author_Id");

				entity.Property(e => e.Created_At).HasColumnName("Created_At");

				entity.Property(e => e.Team_Id).HasColumnName("Team_Id");

				entity.HasOne(d => d.Author)
					.WithMany(p => p.Projects)
					.HasForeignKey(d => d.Author_Id);

				entity.HasOne(d => d.Team)
					.WithMany(p => p.Projects)
					.HasForeignKey(d => d.Team_Id);
			});

			modelBuilder.Entity<Task>(entity =>
			{
				entity.Property(e => e.Id).ValueGeneratedNever();

				entity.Property(e => e.Created_At).HasColumnName("Created_At");

				entity.Property(e => e.Finished_At).HasColumnName("Finished_At");

				entity.Property(e => e.Performer_Id).HasColumnName("Performer_Id");

				entity.Property(e => e.Project_Id).HasColumnName("Project_Id");

				entity.HasOne(d => d.Performer)
					.WithMany(p => p.Tasks)
					.HasForeignKey(d => d.Performer_Id);

				entity.HasOne(d => d.Project)
					.WithMany(p => p.Tasks)
					.HasForeignKey(d => d.Project_Id);
			});

			modelBuilder.Entity<Team>(entity =>
			{
				entity.Property(e => e.Id).ValueGeneratedNever();

				entity.Property(e => e.Created_At).HasColumnName("Created_At");
			});

			modelBuilder.Entity<User>(entity =>
			{
				entity.Property(e => e.Id).ValueGeneratedNever();

				entity.Property(e => e.First_Name).HasColumnName("First_Name");

				entity.Property(e => e.Last_Name).HasColumnName("Last_Name");

				entity.Property(e => e.Registered_At).HasColumnName("Registered_At");

				entity.Property(e => e.Team_Id).HasColumnName("Team_Id");

				entity.HasOne(d => d.Team)
					.WithMany(p => p.Users)
					.HasForeignKey(d => d.Team_Id);
			});
		}

	}

	public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<BinaryDataContext>
	{
		public BinaryDataContext CreateDbContext(string[] args)
		{
			IConfigurationRoot configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile(@Directory.GetCurrentDirectory() + "/../Task4.API/appsettings.json").Build();
			var builder = new DbContextOptionsBuilder<BinaryDataContext>();
			var connectionString = configuration.GetConnectionString("DefaultConnection");
			builder.UseSqlServer(connectionString);
			return new BinaryDataContext(builder.Options);
		}
	}
}
