﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
	//Task4.Lib
	public class Task
	{
		[Required]
		public int Id { get; set; }
		[StringLength(100, MinimumLength = 3), Required]
		public string Name { get; set; }
		[Required]
		public string Description { get; set; }
		[Required]
		public DateTime Created_At { get; set; }
		[Required]
		public DateTime Finished_At { get; set; }
		[Required]
		public TaskState State { get; set; }
		public int? Project_Id { get; set; }
		public int? Performer_Id { get; set; }

		public virtual User Performer { get; set; }
		public virtual Project Project { get; set; }

		public override string ToString()
		{
			return $"Id: {Id}, Name: {Name}, Description: {Description}, Created_At: {Created_At.ToShortDateString()}," +
				   $"Finished_At: {Finished_At}, State: {State}," +
				   $" Project_Id: {Project_Id}, Performer_Id: {Performer_Id}\n";
		}
	}
}
