﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
	public class User
	{
		public User()
		{
			Projects = new HashSet<Project>();
			Tasks = new HashSet<Task>();
		}

		[Required]
		public int Id { get; set; }
		[StringLength(100, MinimumLength = 3), Required]
		public string First_Name { get; set; }
		[StringLength(100, MinimumLength = 3), Required]
		public string Last_Name { get; set; }
		[Required]
		public string Email { get; set; }
		public DateTime Birthday { get; set; }
		public DateTime Registered_At { get; set; }
		public int? Team_Id { get; set; }

		public virtual Team Team { get; set; }
		public virtual ICollection<Project> Projects { get; set; }
		public virtual ICollection<Task> Tasks { get; set; }

		public override string ToString()
		{
			return $"Id {Id}, Name: {First_Name} {Last_Name}, Email: {Email}, Birthday: {Birthday.ToShortDateString()}," +
				   $"Registered_At: {Registered_At.ToShortDateString()}, Team_Id: {Team_Id}\n";
		}
	}
}
