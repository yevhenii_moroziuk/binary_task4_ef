﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
	public class Project
	{
		public Project()
		{
			Tasks = new HashSet<Task>();
		}

		[Required]
		public int Id { get; set; }
		[StringLength(100, MinimumLength = 3), Required]
		public string Name { get; set; }
		[Required]
		public string Description { get; set; }
		[Required]
		public DateTime Created_At { get; set; }
		public int? Author_Id { get; set; }
		public int? Team_Id { get; set; }

		public virtual User Author { get; set; }
		public virtual Team Team { get; set; }
		public virtual ICollection<Task> Tasks { get; set; }

		public override string ToString()
		{
			return $"Id: {Id}, Name: {Name}," +
				   //$" , Description: {Description}," +
				   //$" Created_At: {Created_At.ToShortDateString(),}
				   $"Author_Id: {Author_Id}, Team_Id: {Team_Id}\n";
		}
	}
}
