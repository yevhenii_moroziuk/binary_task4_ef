﻿using System;

namespace DataAccess.Models
{
	public class LogModel
	{
		public DateTime WrittenData { get; set; }
		public string Message { get; set; }
	}
}
