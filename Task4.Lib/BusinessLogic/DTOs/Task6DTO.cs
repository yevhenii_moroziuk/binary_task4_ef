﻿using DataAccess.Models;

namespace BusinessLogic.DTOs

{
	public class Task6DTO
	{
		public User User { get; set; }
		public Project LastProject { get; set; }
		public int TaskCount { get; set; }
		public int UnfinishedTaskCount { get; set; }
		public Task LongestTask { get; set; }

		public Task6DTO(User user, Project project, int count1, int count2, Task task1)
		{
			User = user;
			LastProject = project;
			TaskCount = count1;
			UnfinishedTaskCount = count2;
			LongestTask = task1;
		}
	}
}
