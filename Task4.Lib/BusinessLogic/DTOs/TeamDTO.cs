﻿using System;

namespace BusinessLogic.DTOs
{
	public class TeamDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime? Created_At { get; set; }
	}
}
