﻿using DataAccess.Models;
using System;

namespace BusinessLogic.DTOs
{
	public class TaskDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime Created_At { get; set; }
		public DateTime Finished_At { get; set; }
		public TaskState State { get; set; }
		public int? Project_Id { get; set; }
		public int? Performer_Id { get; set; }
	}
}
