﻿using AutoMapper;
using BusinessLogic.DTOs;
using DataAccess.Models;

namespace Task4.API.Mapping
{
	public class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			//User
			CreateMap<User, UserDTO>().ReverseMap();
			//Team
			CreateMap<Team, TeamDTO>().ReverseMap();
			//Task
			CreateMap<Task, TaskDTO>().ReverseMap();
			//Project
			CreateMap<Project, ProjectDTO>().ReverseMap();
		}
	}
}
