﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task4.API.Services
{
	public interface ICrudService<TEntity>
	{
		void Create(TEntity entity);
		void Update(TEntity entity);
		void Delete(int id);
		List<TEntity> ShowAll();
		TEntity Show(int id);
		bool IsExists(int id);

	}
}
