﻿using AutoMapper;
using BusinessLogic.DTOs;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task4.Lib.DataAccess;

namespace Task4.API.Services
{
	public class TeamService:ICrudService<TeamDTO>
	{
		private readonly BinaryDataContext _context;
		private readonly IMapper _mapper;

		public TeamService(BinaryDataContext source, IMapper mapper)
		{
			_context = source;
			_mapper = mapper;
		}

		public void Create(TeamDTO entity)
		{
			_context.Teams.Add(_mapper.Map<Team>(entity));

			_context.SaveChanges();
		}

		public void Delete(int id)
		{
			var team = _context.Teams.SingleOrDefault(u => u.Id == id);
			_context.Teams.Remove(team);

			_context.SaveChanges();
		}

		public TeamDTO Show(int Id)
		{
			var team = _context.Teams.SingleOrDefault(u => u.Id == Id);
			return _mapper.Map<TeamDTO>(team);
		}

		public List<TeamDTO> ShowAll()
		{
			var teams = _context.Teams;
			return _mapper.Map<List<TeamDTO>>(teams);
		}

		public void Update(TeamDTO entity)
		{
			var team = _context.Teams.SingleOrDefault(u => u.Id == entity.Id);//find this user in collection
			team = _mapper.Map<Team>(entity);

			_context.SaveChanges();
		}

		public bool IsExists(int id)
		{
			return _context.Teams.SingleOrDefault(u => u.Id == id) == null ? false : true;
		}
	}
}
