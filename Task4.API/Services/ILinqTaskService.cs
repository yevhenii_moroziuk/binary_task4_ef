﻿using BusinessLogic.DTOs;
using DataAccess.Models;
using System.Collections.Generic;

namespace Task4.API.Services
{
	public interface ILinqTaskService
	{
		IDictionary<string, int> GetTasksCount(int author);
		List<Task> GetTasks(int user_Id);
		List<string> GetFinishedTasks(int user_Id);
		IDictionary<string, List<User>> GetTeamDefinition();
		IDictionary<User, List<Task>> GetUserTasks();
		Task6DTO GetUserDefinition(int user_id);
		Task7DTO GetProjectDefinition(int project_id);
		List<LogModel> GetLog();
	}
}
