﻿using AutoMapper;
using BusinessLogic.DTOs;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Task4.Lib.DataAccess;

namespace Task4.API.Services
{
	public class UserService:ICrudService<UserDTO>
	{
		private readonly BinaryDataContext _context;
		private readonly IMapper _mapper;

		public UserService(BinaryDataContext source, IMapper mapper)
		{
			_context = source;
			_mapper = mapper;
		}

		public void Create(UserDTO entity)
		{
			//user has Team FK
			if (_context.Teams.SingleOrDefault(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid team Id");

			_context.Users.Add(_mapper.Map<User>(entity));

			_context.SaveChanges();
		}

		public void Delete(int id)
		{
			var user = _context.Users.SingleOrDefault(u => u.Id == id);
			_context.Users.Remove(user);

			_context.SaveChanges();
		}

		public UserDTO Show(int Id)
		{
			var user = _context.Users.SingleOrDefault(u => u.Id == Id);
			return _mapper.Map<UserDTO>(user);
		}

		public List<UserDTO> ShowAll()
		{
			var users = _context.Users;
			return _mapper.Map<List<UserDTO>>(users);
		}

		public void Update(UserDTO entity)
		{
			//user has Team FK
			if (_context.Teams.SingleOrDefault(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid team Id");

			var user = _context.Users.SingleOrDefault(u => u.Id == entity.Id);//find this user in collection
			user = _mapper.Map<User>(entity);

			_context.SaveChanges();
		}

		public bool IsExists(int id)
		{
			return _context.Users.SingleOrDefault(u => u.Id == id) == null ? false : true;
		}
	}
}
