﻿using AutoMapper;
using BusinessLogic.DTOs;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Task4.Lib.DataAccess;

namespace Task4.API.Services
{
	public class TaskService : ICrudService<TaskDTO>
	{
		private readonly BinaryDataContext _context;
		private readonly IMapper _mapper;

		public TaskService(BinaryDataContext source, IMapper mapper)
		{
			_context = source;
			_mapper = mapper;
		}

		public void Create(TaskDTO entity)
		{
			//Task has Project & Team FK
			if (_context.Users.SingleOrDefault(u => u.Id == entity.Performer_Id) == null ||
				_context.Projects.SingleOrDefault(p => p.Id == entity.Project_Id) == null)
				throw new Exception("Invalid PerformerId or ProjectID");

			_context.Tasks.Add(_mapper.Map<Task>(entity));

			_context.SaveChanges();
		}

		public void Delete(int id)
		{
			var task = _context.Tasks.SingleOrDefault(u => u.Id == id);
			_context.Tasks.Remove(task);

			_context.SaveChanges();
		}

		public TaskDTO Show(int Id)
		{
			var task = _context.Tasks.SingleOrDefault(u => u.Id == Id);
			return _mapper.Map<TaskDTO>(task);
		}

		public List<TaskDTO> ShowAll()
		{
			var tasks = _context.Tasks;
			return _mapper.Map<List<TaskDTO>>(tasks);
		}

		public void Update(TaskDTO entity)
		{
			//Task has Project & Team FK
			if (_context.Users.SingleOrDefault(u => u.Id == entity.Performer_Id) == null ||
				_context.Projects.SingleOrDefault(p => p.Id == entity.Project_Id) == null)
				throw new Exception("Invalid PerformerId or ProjectID");

			var user = _context.Tasks.SingleOrDefault(u => u.Id == entity.Id);//find this user in collection
			user = _mapper.Map<Task>(entity);

			_context.SaveChanges();
		}

		public bool IsExists(int id)
		{
			return _context.Tasks.SingleOrDefault(u => u.Id == id) == null ? false : true;
		}
	}
}