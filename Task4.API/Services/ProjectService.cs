﻿using AutoMapper;
using BusinessLogic.DTOs;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Task4.Lib.DataAccess;

namespace Task4.API.Services
{
	public class ProjectService : ICrudService<ProjectDTO>
	{
		private readonly BinaryDataContext _context;
		private readonly IMapper _mapper;

		public ProjectService(BinaryDataContext source, IMapper mapper)
		{
			_context = source;
			_mapper = mapper;
		}

		public void Create(ProjectDTO entity)
		{
			//Project has User & Team FK
			if (_context.Users.SingleOrDefault(u => u.Id == entity.Author_Id) == null ||
				_context.Teams.SingleOrDefault(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid AuthorId or TeamId");


			_context.Projects.Add(_mapper.Map<Project>(entity));

			_context.SaveChanges();
		}

		public void Delete(int id)
		{
			var projects = _context.Projects.SingleOrDefault(u => u.Id == id);
			_context.Projects.Remove(projects);

			_context.SaveChanges();
		}

		public ProjectDTO Show(int Id)
		{
			var project = _context.Projects.SingleOrDefault(u => u.Id == Id);
			return _mapper.Map<ProjectDTO>(project);
		}

		public List<ProjectDTO> ShowAll()
		{
			var projects = _context.Projects;
			return _mapper.Map<List<ProjectDTO>>(projects);
		}

		public void Update(ProjectDTO entity)
		{
			//Project has User & Team FK
			if (_context.Users.SingleOrDefault(u => u.Id == entity.Author_Id) == null ||
				_context.Teams.SingleOrDefault(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid AuthorId or TeamId");


			var projects = _context.Projects.SingleOrDefault(u => u.Id == entity.Id);//find this user in collection
			projects = _mapper.Map<Project>(entity);

			_context.SaveChanges();
		}

		public bool IsExists(int id)
		{
			return _context.Projects.SingleOrDefault(u => u.Id == id) == null ? false : true;
		}
	}
}