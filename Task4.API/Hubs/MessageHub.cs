﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Task4.API.Hubs
{
	public class MessageHub:Hub
	{
		public async Task SendMessage(string message)
		{
			await Clients.All.SendAsync("BroadcastMessage", message);
		}
	}
}
