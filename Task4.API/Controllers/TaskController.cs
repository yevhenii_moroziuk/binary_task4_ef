﻿using System.Collections.Generic;
using BusinessLogic.DTOs;
using Microsoft.AspNetCore.Mvc;
using Task4.API.Services;

namespace Task4.API.Controllers
{
	[Route("api/tasks")]
    [ApiController]
    public class TaskController : ControllerBase
	{
		private readonly ICrudService<TaskDTO> _service;

		public TaskController(ICrudService<TaskDTO> service)
		{
			_service = service;
		}

		[HttpGet]
		public ActionResult<List<TaskDTO>> GetTasks()
		{
			return _service.ShowAll();
		}

		[HttpGet("{id}")]
		public IActionResult GetTask(int id)
		{
			if (!_service.IsExists(id))
				return NotFound();

			return Ok(_service.Show(id));
		}

		[HttpPost]
		public IActionResult PostTask(TaskDTO taskDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (_service.IsExists(taskDTO.Id))
				return BadRequest();

			try
			{
				_service.Create(taskDTO);
				return Created("api/tasks", taskDTO);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid data");
			}
		}

		[HttpPut]
		public IActionResult EditTask(TaskDTO entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid data");

			if (!_service.IsExists(entity.Id))
				return NotFound();

			try
			{
				_service.Update(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("Invalid data");
			}
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteTasks(int id)
		{
			if (!_service.IsExists(id))
				return BadRequest();

			_service.Delete(id);

			return Ok("deleted");
		}
	}
}