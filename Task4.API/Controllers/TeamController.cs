﻿using System.Collections.Generic;
using BusinessLogic.DTOs;
using Microsoft.AspNetCore.Mvc;
using Task4.API.Services;

namespace Task4.API.Controllers
{
	[Route("api/teams")]
    [ApiController]
    public class TeamController : ControllerBase
	{
		private readonly ICrudService<TeamDTO> _service;

		public TeamController(ICrudService<TeamDTO> service)
		{
			_service = service;
		}

		[HttpGet]
		public ActionResult<List<TeamDTO>> GetTeams()
		{
			return _service.ShowAll();
		}

		[HttpGet("{id}")]
		public IActionResult GetTeam(int id)
		{
			if (!_service.IsExists(id))
				return NotFound();

			return Ok(_service.Show(id));
		}

		[HttpPost]
		public IActionResult PostTeam(TeamDTO teamDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (_service.IsExists(teamDTO.Id))
				return BadRequest();

			try
			{
				_service.Create(teamDTO);
				return Created("api/teams", teamDTO);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid data");
			}
		}

		[HttpPut]
		public IActionResult EditTeam(TeamDTO entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid data");

			if (!_service.IsExists(entity.Id))
				return NotFound();

			try
			{
				_service.Update(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("Invalid data");
			}
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteTeam(int id)
		{
			if (!_service.IsExists(id))
				return BadRequest();

			_service.Delete(id);

			return Ok("deleted");
		}
	}
}