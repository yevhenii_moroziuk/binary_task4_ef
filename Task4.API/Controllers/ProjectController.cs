﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Task4.API.Services;

namespace Task4.API.Controllers
{
	[Route("api/projects")]
	[ApiController]
	public class ProjectController : ControllerBase
	{
		private readonly ICrudService<ProjectDTO> _service;

		public ProjectController(ICrudService<ProjectDTO> service)
		{
			_service = service;
		}

		[HttpGet]
		public ActionResult<List<ProjectDTO>> GetProjects()
		{
			return _service.ShowAll();
		}

		[HttpGet("{id}")]
		public IActionResult GetProject(int id)
		{
			if (!_service.IsExists(id))
				return NotFound();

			return Ok(_service.Show(id));
		}

		[HttpPost]
		public IActionResult PostProject(ProjectDTO projectDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (_service.IsExists(projectDTO.Id))
				return BadRequest();

			try
			{
				_service.Create(projectDTO);
				return Created("api/projects", projectDTO);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid data");
			}
		}

		[HttpPut]
		public IActionResult EditProject(ProjectDTO entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid data");

			if (!_service.IsExists(entity.Id))
				return NotFound();

			try
			{
				_service.Update(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("Invalid data");
			}
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteProjects(int id)
		{
			if (!_service.IsExists(id))
				return BadRequest();

			_service.Delete(id);

			return Ok("deleted");
		}
	}
}