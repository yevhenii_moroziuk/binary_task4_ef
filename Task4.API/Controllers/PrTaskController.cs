﻿using System.Collections.Generic;
using BusinessLogic.DTOs;
using DataAccess.Models;
using Microsoft.AspNetCore.Mvc;
using Task4.API.Queue;
using Task4.API.Services;

namespace Task4.API.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class PrTaskController : ControllerBase
    {
		private ILinqTaskService _lts;
		private readonly QueueService _service;

		public PrTaskController(ILinqTaskService taskRepository, QueueService service)
		{
			_lts = taskRepository;
			_service = service;
		}

		[HttpGet("task1/{author}")]
		public IDictionary<string, int> Task1(int author)
		{
			_service.TransferMessage("task1 started");

			return _lts.GetTasksCount(author);
		}

		[HttpGet("task2/{userid}")]
		public ActionResult<List<Task>> Task2(int userid)
		{
			_service.TransferMessage("task2 started");

			return _lts.GetTasks(userid);
		}

		[HttpGet("task3/{userid}")]
		public ActionResult<List<string>> Task3(int userid)
		{
			_service.TransferMessage("task3 started");

			return _lts.GetFinishedTasks(userid);
		}

		[HttpGet("task4/")]
		public IDictionary<string, List<User>> Task4()
		{
			_service.TransferMessage("task4 started");

			return _lts.GetTeamDefinition();
		}

		[HttpGet("task5/")]
		public IDictionary<User, List<Task>> Task5()
		{
			_service.TransferMessage("task5 started");

			return _lts.GetUserTasks();
		}
		[HttpGet("task6/{userid}")]
		public ActionResult<Task6DTO> Task6(int userid)
		{
			_service.TransferMessage("task6 started");

			return _lts.GetUserDefinition(userid);
		}

		[HttpGet("task7/{projectid}")]
		public ActionResult<Task7DTO> Task7(int projectid)
		{
			_service.TransferMessage("task7 started");

			return _lts.GetProjectDefinition(projectid);
		}

		[HttpGet("log")]
		public List<LogModel> Log()
		{
			return _lts.GetLog();
		}
	}
}