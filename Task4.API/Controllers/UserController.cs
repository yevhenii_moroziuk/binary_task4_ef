﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BusinessLogic.DTOs;
using Microsoft.AspNetCore.Mvc;
using Task4.API.Services;

namespace Task4.API.Controllers
{
	[Route("api/users")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private readonly ICrudService<UserDTO> _service;

		public UserController(ICrudService<UserDTO> service)
		{
			_service = service;
		}

		[HttpGet]
		public ActionResult<List<UserDTO>> GetUsers()
		{
			return _service.ShowAll();
		}

		[HttpGet("{id}")]
		public IActionResult GetUser(int id)
		{
			if (!_service.IsExists(id))
				return NotFound();

			return Ok(_service.Show(id));
		}

		[HttpPost]
		public IActionResult PostUser(UserDTO userDTO)//(UserDTO userDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (_service.IsExists(userDTO.Id))
				return BadRequest();

			try
			{
				_service.Create(userDTO);
				return Created("api/users", userDTO);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid team id");
			}
		}

		[HttpPut]
		public IActionResult EditUser(UserDTO entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid user id");

			if (!_service.IsExists(entity.Id))
				return NotFound();

			try
			{
				_service.Update(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("Invalid team id");
			}
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteUser(int id)
		{
			if (!_service.IsExists(id))
				return BadRequest();

			_service.Delete(id);

			return Ok("deleted");
		}
	}
}